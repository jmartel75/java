package com.veracode.demo;

import org.springframework.web.util.HtmlUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    private static final String template = "{\"message\": \"Greetings from Veracode, %s!\"}";

    @RequestMapping("/greeting")
    public String index(@RequestParam(value = "name", required = false, defaultValue = "User") String name) {
        return String.format(template, HtmlUtils.htmlEscape(name));
    }

}
